import sqlite3
import csv

def connect_to_db(dbname):
    """ Establish a connection to the database file.
    :param dbname: the name of the database file.
    :return: the connection object for the database file
    """
    conn = sqlite3.connect(dbname)
    return conn

def create_db_table(dbname):
    """ Attempt to create an empty table in the database file, with error handling.
    :param dbname: the name of the database file.
    """
    try:
        conn = connect_to_db(dbname)
        conn.execute('''
            CREATE TABLE art (
                id INTEGER PRIMARY KEY NOT NULL,
                art_name VARCHAR(32),
                artist VARCHAR(32),
                medium VARCHAR(32),
                year_created INTEGER,
                location VARCHAR(32),
                image_path VARCHAR(32)
            );
        ''')

        conn.commit()
        print("Table create successfully")
    except sqlite3.Error as e:
        print("Error:", e)
        print("Table creation failed")
        print()
    finally:
        conn.close()

def csv_to_db(csvname, dbname):
    """ Read data from a csv file into the database.
    """
    try:
        conn = connect_to_db(dbname)
        try:
            with open(csvname) as in_csv:
                # Create csv reader object
                reader = csv.reader(in_csv)

                # Extract field row at top of csv file
                fields = next(reader)

                # Read the data from the csv file, row by row, insert into the database
                sql = '''INSERT INTO art (id, art_name, artist, medium, year_created, location, image_path) 
                            VALUES (?, ?, ?, ?, ?, ?, ?)'''
                cur = conn.cursor()
                cur.executemany(sql, reader)
        except:
            conn.rollback()
            print("Error reading csv data into database, rolling back")
    except:
        print("CSV reading failed")
    finally:
        conn.commit()
        conn.close()

def insert_art(art):
    """ Implements the 'Create' in CRUD - inserts a row into the database.
    :param art: the art piece to be inserted
    :return: the art piece that was inserted
    """
    result = {}
    try:
        conn = connect_to_db()
        # Cursor object is used to execute CRUD operations
        cur = conn.cursor()
        # SQL 'INSERT INTO' statement inserts new records into a database
        cur.execute("INSERT INTO art (id, art_name, artist, medium, year_created, location, image_path) \
                        VALUES (?, ?, ?, ?, ?, ?)", (art['id'], art['art_name'], art['artist'],
                        art['medium'], art['year_created'], art['location'], art['image_path']))
        # Changes must be committed to the database
        conn.commit()
        result = get_art(cur.lastrowid)
    except:
        # If an error occurs, roll back and state which function the error occurred in
        conn.rollback()
        print("Error inserting new art piece into the database")
    
    finally:
        # Always remember to close the connection when done
        conn.close()
    
    return result
        
def get_all_art():
    """ Partially implements the 'Retrieve' in CRUD - retrieves ALL art pieces from the database
    :return: all the art in the database
    """
    result = []
    conn = connect_to_db("art.db")
    try:
        
        # row_factory = Row makes it so that name-based access to columns are possible rather than just indexed like with tuples
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        # SQL SELECT statement to retrieve everything from the database
        cur.execute("SELECT * FROM art")
        rows = cur.fetchall()

        # convert row objects to dictionary
        for i in rows:
            art = {}
            art['id'] = i['id']
            art['art_name'] = i['art_name']
            art['artist'] = i['artist']
            art['medium'] = i['medium']
            art['year_created'] = i['year_created']
            art['location'] = i['location']
            art['image_path'] = i['image_path']
            result.append(art)
            
    except:
        result = []
        print("Error retrieving art pieces from the database")
    
    finally:
        conn.close()
    
    return result

def get_art(name):
    """ Partially implements the 'Retrieve' in CRUD - retrieves an art piece from the database by name
    :param name: the name of the art piece to be retrieved
    :return: the specified art piece's database entry
    """
    result = {}
    try:
        conn = connect_to_db()
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        # SQL SELECT statement along with WHERE to retrieve an art piece with a certain name.
        cur.execute("SELECT * FROM art WHERE name = ?", (name))
        row = cur.fetchone()

        # convert row object to dictionary
        art['id'] = row['id']
        art['art_name'] = row['art_name']
        art['artist'] = row['artist']
        art['medium'] = row['medium']
        art['year_created'] = row['year_created']
        art['location'] = row['location']
        art['image_path'] = row['image_path']
    
    except:
        result = {}
        print("Error retrieving art piece from the database with name:", name)
    
    finally:
        conn.close()

    return result

def update_art(art):
    """ Implements the 'Update' in CRUD - updates a single art piece in the database
    :param art: the art piece to be updated (identified by name)
    :return: the art piece that was updated
    """
    result = {}
    try:
        conn = connect_to_db()
        cur = conn.cursor()
        # SQL UPDATE statement along with WHERE to update an art piece with a certain name.
        cur.execute("UPDATE art SET id = ?, art_name = ?, artist = ?, medium = ?, year_created = ?\
                     , location = ?, image_path = ? WHERE art_name = ?",
                     (art['id'], art['art_name'], art['artist'],
                     art['medium'], art['year_created'], art['location'], art['image_path']))
        conn.commit()
        # return the art info
        result = get_art(art['art_name'])
    
    except sqlite3.Error as e:
        conn.rollback()
        result = {}
        print("Error:", e)
        print("There was an issue updating the art piece:", art)
    
    finally:
        conn.close()
    
    return result

def delete_art(name):
    """ Implements the 'Delete' in CRUD - deletes a single art piece in the database
    :param name: the name of the art piece to delete
    :return: a success/failure message
    """
    message = {}
    try:
        conn = connect_to_db()
        # SQL DELETE statement along with WHERE to delete an art piece with a certain name. 
        conn.execute("DELETE from art WHERE name = ?", (name))
        conn.commit()
        message['status'] = "Art piece deleted successfully"
    except:
        conn.rollback()
        message['status'] = "Cannot delete art piece"
    finally:
        conn.close()
    
    return message
