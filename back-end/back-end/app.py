from flask import Flask, request, jsonify
from flask_cors import CORS
import db

dbname = "art.db"
csv = "Art.csv"

# Connect to the database art.db
db.connect_to_db(dbname)
# Create the table in the database file
db.create_db_table(dbname)
# Add the data from the csv to the database
db.csv_to_db(csv, dbname)

app = Flask(__name__)
# Allow access to endpoints from any IP address
CORS(app, resources={r"/*": {"origins": "*"}})

@app.route('/')
def index():
    return """Welcome to the Art Gallery REST API. Here are the utilities available to you:<br>
    \t* /api/art: Return all art pieces using a GET request<br>
    \t* /api/art/<name>: Return an art piece using a GET request (via name of piece)<br>
    \t* /api/art/add: Add an art piece using a POST request (new data should be in JSON form)<br>
    \t* /api/art/update: Update an art piece using a PUT request (updates made by linking name)<br>
    \t* /api/art/delete/<name>: Delete an art piece using a DELETE request (via name of piece)<br>
    """

@app.route('/api/art', methods=['GET'])
def api_get_all_art():
    # We use jsonify so that we are returning JSON objects rather than Python dictionaries
    return jsonify(db.get_all_art())

@app.route('/api/art/<string:name>', methods=['GET']) 
def api_get_art(name):
    return jsonify(db.get_art(name))

@app.route('/api/art/add', methods=['POST'])
def api_insert_art():
    name = request.get_json()
    result = db.insert_art(name)
    return jsonify(result)

@app.route('/api/art/update', methods=['PUT'])
def api_update_art():
    name = request.get_json()
    result = db.update_art(name)
    return jsonify(result)

@app.route('/api/art/delete/<string:name>', methods=['DELETE'])
def api_delete_art(name):
    return jsonify(db.delete_art(name))

if __name__ == "__main__":
    #app.debug = True
    #app.run(debug=True)
    app.run() # run Flask app

