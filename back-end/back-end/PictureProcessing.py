"""
Author: Nick Hanna; Jialong Ke; Marit Lebakken; Adalia M Williams 
For: Art Gallery - Final Project
Date: September 9th, 2022
Description:
  This program gets basic metadata, and exif metadata if it exists.
  https://pillow.readthedocs.io/en/stable/_modules/PIL/Image.html
  https://sighack.com/post/averaging-rgb-colors-the-right-way#:~:text=The%20typical%20approach%20to%20averaging,components%20of%20the%20final%20color. 
  https://10015.io/tools/image-average-color-finder 
"""

from PIL import Image
from PIL.ExifTags import TAGS
import math
import pprint

from cv2 import sqrt

def average(color_array):
  """Averages the values of an array linearly and returns the averaged value
  :param color list, i.e. list of either R, G, or B values
  :returns the average of list
  """
  average_color = 0
  for i in color_array:
    average_color += i
  return round(average_color / len(color_array))


def get_metadata(image):
  """Takes image object and returns list of metadata
  :param PILLOW image object
  :returns dictionary of various elements metadata, specified in definition
  """
  # extract basic metadata
  if not image:
    print("No Good Metadata")
    return 0
  else:
    info_dict = {
        "Filename": image.filename,
        "Image Size": image.size,
        "Image Height": image.height,
        "Image Width": image.width,
        "Image Format": image.format,
        "Image Mode": image.mode,
        "Image Pallette": image.palette,
        "Image is Animated": getattr(image, "is_animated", False),
        "Frames in Image": getattr(image, "n_frames", 1)
    }
  # # print base info dict
  # for label,value in info_dict.items():
  #     print(f"{label:25}: {value}")
  return info_dict


def get_exif_metadata(img_exif):
  """Takes image object and returns list of exif metadata
  :param PILLOW image object
  :returns dictionary of various elements metadata, anything acessible in the TAGS dictionary element included in PIL library
  """
  # Print if no good exif image metadata
  if not img_exif:
    print("No Good Exif Metadata")
    return 0
  else:
    exif_table={}
    # load any image exif tag into exif table
    for index, item in img_exif:
      tag = TAGS.get(index)
      exif_table[tag] = item
    # pprint.pprint(exif_table)
  return exif_table


def get_average_rgb_m1(image,img_metadata):
  """Takes image type and image metadata and finds the average pixel color.
  This uses the simple average technique and not the square root technique
  TODO THIS METHOD IS EXTREMELY COSTLY, TEST ON SERVER AND SEE IF IT IS FEASABLE TO IMPLEMENT
  :param PIL image object and specialized metadata dict
  :returns Tuple containing Brute method RGB values
  """
  red = []
  green = []
  blue = []
  average_rgb = ()

  height = img_metadata.get("Image Height")
  width = img_metadata.get("Image Width")

  for y in range(height):
    for x in range(width):
      r, g, b = image.getpixel((x,y))
      red.append(r)
      green.append(g)
      blue.append(b)

  average_rgb = (average(red),average(green),average(blue))
  return average_rgb


def get_average_rgb_m2(image,img_metadata):
  """
  This Second method returns the sqrt of the mean of the squared RGB Value, 
  this returns a brighter value than m1, and is considered to be more accurate
  This is known as the sqrt method. but probably not faster than m1. 
  :param PIL image object and specialized metadata dict
  :returns Tuple containing Brute method RGB values
  """
  red = 0
  green = 0
  blue = 0
  average_rgb = ()

  height = img_metadata.get("Image Height")
  width = img_metadata.get("Image Width")

  counter = 0
  for y in range(height):
    for x in range(width):
      r, g, b = image.getpixel((x,y))
      red += (r * r)
      green += (g * g)
      blue += (b * b)
      counter += 1

  average_rgb = (round(math.sqrt(red/counter)),round(math.sqrt(green/counter)),round(math.sqrt(blue/counter)))
  return average_rgb


def main():
    # path to image/video
  imagename = r'C:\Users\nhann\cs-205-final-project-art-gallery\Art Pictures\cat.jpg'

  # read the image data using PIL
  image = Image.open(imagename)

  # extract any image metadata
  img_exif = image.getexif().items()

  # variable containing image metadata
  img_metadata = get_metadata(image)
  pprint.pprint(img_metadata)

  print("-----------")

  img_exif_metadata = get_exif_metadata(img_exif)
  pprint.pprint(img_exif_metadata)

  print("-----------")

  # Get and print RGB brute method average
  # average_rgb = get_average_rgb_m1(image, img_metadata)
  # pprint.pprint(average_rgb)

  # Get and print RGB method 2
  average_rgb2 = get_average_rgb_m2(image, img_metadata)
  print(average_rgb2)


if __name__ == '__main__':
  main()
