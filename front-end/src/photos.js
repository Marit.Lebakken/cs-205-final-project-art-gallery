// https://codesandbox.io/s/5vn3lvz2n4?file=/photos.js:0-898

export const photos = [
    {
      src: "https://collectionapi.metmuseum.org/api/collection/v1/iiif/335617/747355/main-image",
      width: 4,
      height: 3
    },
    {
      src: "https://collectionapi.metmuseum.org/api/collection/v1/iiif/200892/405564/main-image",
      width: 1,
      height: 1
    },
    {
      src: "https://collectionapi.metmuseum.org/api/collection/v1/iiif/39666/154223/main-image",
      width: 3,
      height: 4
    },
    {
      src: "https://collectionapi.metmuseum.org/api/collection/v1/iiif/10154/34358/main-image",
      width: 3,
      height: 4
    },
    {
      src: "https://collectionapi.metmuseum.org/api/collection/v1/iiif/81108/261848/main-image",
      width: 3,
      height: 4
    },
    {
      src: "https://collectionapi.metmuseum.org/api/collection/v1/iiif/544502/1220516/main-image",
      width: 4,
      height: 3
    },
    {
      src: "https://collectionapi.metmuseum.org/api/collection/v1/iiif/436535/796067/main-image",
      width: 3,
      height: 4
    },

  ];
