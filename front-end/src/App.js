/*
FILE NAME: App.js
For: CS-205-FINAL-PROJECT-ART-GALLERY
    Front End
Authors: 
    Nick Hanna, Jialong Ke, Marit Lebakken, Adalia Williams
Description:
    This is the file for App Component. 
    App Component is the main component in React which acts as a container for all other components.
    It is the most important piece of our project, connects to the MET API, and displays it. 
*/

// https://metmuseum.github.io/
// https://www.postman.com/opamcurators/workspace/open-access-museums/example/1501710-5fe6a964-cc25-40bf-92fe-74a028481850
import React, { useState } from "react";
import "./App.css";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Toolbar from '@mui/material/Toolbar';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Typography from '@mui/material/Typography';
import { BarsOutlined } from "@ant-design/icons";
import { Image, Drawer, Col, Row } from "antd";

import axios from 'axios';

function App() {
    const [userInput, setUserInput] = useState("");
    const [open, setOpen] = useState(false);
    const [show, setShow] = useState(false);
    const [content, setContent] = useState([]);
    // SelectedArt state after click it renders on right 
    const [selectedArt, setSelectedArt]= useState({});
    
    const searchContent = () => {
        searchArt(userInput);
        console.log(content);
    };

    // Selected Value stored
    const showDrawer = (res) => {
        setSelectedArt(res);
        setOpen(true);
        
    };
    const onClose = () => {
        setOpen(false);
    };

    /*Use axios to query the objects, Fetch data, index into JSON sub elements
    package to help query data and prevent errors that raw fetch may cause
    @ return FetchData.data.data
    */
    const fetchInfoFromId = async(objectId) => {

        let FetchData = await axios.get(`https://collectionapi.metmuseum.org/public/collection/v1/objects/${objectId}`)

        console.log('fetchData ', FetchData); 

        if(FetchData.data.data.primaryImage){
            return FetchData.data.data
        }
       
        /* 
        // Previous Fetch Technique, kept for reference 
        let url = "https://collectionapi.metmuseum.org/public/collection/v1/objects/" + objectId
         let data = null;
         fetch(url)
         .then(async response => {
             data = await response.json();  // parses body text returns as JS object
             // check for error response
             if(data){
                 if(data.primaryImage){
                     setContent(a => {
                         a.push(data.primaryImage);
                         return data;
                     });
                     return data;
                 }
             }
             // this.setState({ totalReactPackages: data.total })
         })
         .catch(error => {
         //     console.error('There was an error!', error);
         })
         ;*/ 
    }

    /*Search art function. Use user input to search the art, fetch url. then wait for corresponding.json files*/
    const searchArt = (userInput) => {
        let image = [];
        let objectInfo = null;

        // Use search query, and returns object ID
        let url = "https://collectionapi.metmuseum.org/public/collection/v1/search?q=" + userInput 
        fetch(url)
        .then(async response => {            
            // Stage was running one stage behind 
            const data = await response.json();  // parses body text returns as JS object
            // check for error response

            console.log('data recieved ', data);
            
            let getData = [];

            // Generate list of image URLs from search result
            console.log('the total time api hit is ', data.objectIDs.length);
            for (let objectId in data.objectIDs) {
                let FetchData = await axios.get(`https://collectionapi.metmuseum.org/public/collection/v1/objects/${data.objectIDs[objectId]}`).then((res)=>{
                        // console.log('res ', res);
                    if(res.data?.primaryImage){     // if there is no data, it wont break, and wont display: allows us to continue code while data.objectIDs[objectId] is still filled
                        getData.push(res.data)
                        console.log('fetching data found ',)
                    }
                    else{
                        console.log('fetching data not found ',)
                    }
                       
                }).catch((err)=>{
                    // console.log('err ', err);
                })

                // in order to change value of array, must store values in seperate variable, line 95
                // get data is filter, contains all the primary Items
                // load 5 pictures at a time - through getData buffer so it render in pieces on page  
                if((getData.length % 5) == 0){
                    setContent([...getData]); 
                    // console.log('zee ', getData); // Debugging..
                }

                /* Previous approach, kept for reference
                console.log('data changes ', content, content.length) // Debugging...            
                let temp = await fetchInfoFromIds(data.objectIDs[objectId]) // Get detailed info including image URL 
                console.log('temp ', temp);
                getData.push(temp);
                */
            }
            console.log('getData --> ', getData);
            // this.setState({ totalReactPackages: data.total })
        })
        .catch(error => {
            // console.error('There was an error!', error);
        });
        
    };
    const onChange = (checkedValues) => {
        console.log("checked = ", checkedValues);
    };
    
    /*
        This Return Value is what actually makes up the page
        Certain elements have been commented out from previous iterations; kept for reference. 
        In the drawer, I could have included a lot more information, but I elected to play it safe and display certain element.
    */
    return (
        <div className="w-100 h-100 home_page">
            <div>
                <Box sx={{ flexGrow: 1 }}>
                    <AppBar position="static" sx={{ bgcolor: "#293A47", }}>
                        <Toolbar variant="regular">
                            <Typography variant="h4" color="inherit" component="div">
                                Art Gallery
                            </Typography>
                        </Toolbar>
                    </AppBar>
                </Box>
            </div>

            <div
                style={{
                    width: "100%",
                    height: "100%",
                }}
            >
                <div
                    style={{
                        width: "100%",
                        height: "100%",
                        paddingTop: "3rem",
                    }}
                >
                    <div className="w-100" style={{ display: "flex", flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                        <div>
                            <TextField
                                label="Enter search"
                                id="filled-size-normal"
                                defaultValue="Normal"
                                variant="filled"
                                className="search_input"
                                value={userInput}
                                onChange={(e) => {
                                    setUserInput(e.target.value);
                                }}
                                sx={{ input: { color: 'white' } }}
                            />
                            {/* <input
                                className="search_input"
                                value={userInput}
                                onChange={(e) => {
                                    setUserInput(e.target.value);
                                }}
                            /> */}
                        </div>
                        <div>
                            <ButtonGroup variant="contained" aria-label="outlined primary button group">
                                <Button onClick={() => {
                                    setShow((e) => !e);
                                }}>
                                    <BarsOutlined className="option_icon" />
                                </Button>
                                <Button className="search_btn" onClick={searchContent}>
                                    Search
                                </Button>
                            </ButtonGroup>
                            {/* <button
                                style={{ backgroundColor: "transparent", borderRadius: "10px", border:"2px solid white" }}
                                onClick={() => {
                                    setShow((e) => !e);
                                }}
                            >
                                <BarsOutlined className="option_icon" />
                            </button> */}
                        </div>

                        {/* <div>
                            <button className="search_btn" onClick={searchContent}>
                                Search
                            </button>
                        </div> */}
                    </div>
                    <div
                        style={{
                            width: "100%",
                            height: "100%",
                            marginTop: "2rem",
                            position: "relative",
                        }}
                    >
                        <div style={{ width: "100%", display: "flex", justifyContent: "center" }}>
                            {show ? (
                                <FormGroup onChange={onChange} sx={{ color: 'white' }}>
                                    <FormControlLabel control={<Checkbox sx={{color: 'white',
                                                                            '&.Mui-checked': {
                                                                            color: 'success', },
                                        }}/>} label="Title" />
                                    <FormControlLabel control={<Checkbox sx={{color: 'white',
                                                                            '&.Mui-checked': {
                                                                            color: 'success', },
                                        }}/>} label="Artist" />
                                    <FormControlLabel control={<Checkbox sx={{color: 'white',
                                                                            '&.Mui-checked': {
                                                                            color: 'success', },
                                        }}/>} label="Medium" />
                                    <FormControlLabel control={<Checkbox sx={{color: 'white',
                                                                            '&.Mui-checked': {
                                                                            color: 'success', },
                                        }}/>} label="Style" />
                                </FormGroup>
                                // <Checkbox.Group
                                //     style={{
                                //         width: "25%",
                                //         color: "white",
                                //         fontWeight: 500,
                                //         textAlign: "left",
                                //     }}
                                //     onChange={onChange}
                                // >
                                //     <div>
                                //         <div>
                                //             <Checkbox value="Background"><span style={{color:"white"}}>Background</span></Checkbox>
                                //         </div>
                                //         <div>
                                //             <Checkbox value="Latest"><span style={{color:"white"}}>Latest</span></Checkbox>
                                //         </div>
                                //         <div>
                                //             <Checkbox value="Most Viewed"> <span style={{color:"white"}}>Most Viewed</span></Checkbox>
                                //         </div>
                                //     </div>
                                // </Checkbox.Group>
                            ) : null}
                        </div>
                        <div
                            className="w-100"
                            style={{
                                overflowY: "scroll",
                                color: "white",
                                height: "70%",
                                padding: "3rem",
                            }}
                        >
                            <Row>
                            {content ? (
                                content.map((res, idx) => (
                                    <Col key={idx} style={{ padding: "1rem" }}>
                                        <img
                                            width={200}
                                            height={200}
                                            onClick={()=>showDrawer(res)}
                                            src={res.primaryImage}
                                            alt="example"
                                            style={{cursor:"pointer"}}
                                        />
                                    </Col>
                                ))
                            ) : (
                                <Image
                                    width={200}
                                    height={200}
                                    src="error"
                                    fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
                                />
                            )}
                            </Row>
                        </div>
                        <Drawer title="Image Name" placement="right" onClose={onClose} open={open}>
                            <p>Preview</p>
                            <Image
                                    width={200}
                                    height={200}
                                    src={selectedArt.primaryImage}
                                    fallback="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMIAAADDCAYAAADQvc6UAAABRWlDQ1BJQ0MgUHJvZmlsZQAAKJFjYGASSSwoyGFhYGDIzSspCnJ3UoiIjFJgf8LAwSDCIMogwMCcmFxc4BgQ4ANUwgCjUcG3awyMIPqyLsis7PPOq3QdDFcvjV3jOD1boQVTPQrgSkktTgbSf4A4LbmgqISBgTEFyFYuLykAsTuAbJEioKOA7DkgdjqEvQHEToKwj4DVhAQ5A9k3gGyB5IxEoBmML4BsnSQk8XQkNtReEOBxcfXxUQg1Mjc0dyHgXNJBSWpFCYh2zi+oLMpMzyhRcASGUqqCZ16yno6CkYGRAQMDKMwhqj/fAIcloxgHQqxAjIHBEugw5sUIsSQpBobtQPdLciLEVJYzMPBHMDBsayhILEqEO4DxG0txmrERhM29nYGBddr//5/DGRjYNRkY/l7////39v///y4Dmn+LgeHANwDrkl1AuO+pmgAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAwqADAAQAAAABAAAAwwAAAAD9b/HnAAAHlklEQVR4Ae3dP3PTWBSGcbGzM6GCKqlIBRV0dHRJFarQ0eUT8LH4BnRU0NHR0UEFVdIlFRV7TzRksomPY8uykTk/zewQfKw/9znv4yvJynLv4uLiV2dBoDiBf4qP3/ARuCRABEFAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghggQAQZQKAnYEaQBAQaASKIAQJEkAEEegJmBElAoBEgghgg0Aj8i0JO4OzsrPv69Wv+hi2qPHr0qNvf39+iI97soRIh4f3z58/u7du3SXX7Xt7Z2enevHmzfQe+oSN2apSAPj09TSrb+XKI/f379+08+A0cNRE2ANkupk+ACNPvkSPcAAEibACyXUyfABGm3yNHuAECRNgAZLuYPgEirKlHu7u7XdyytGwHAd8jjNyng4OD7vnz51dbPT8/7z58+NB9+/bt6jU/TI+AGWHEnrx48eJ/EsSmHzx40L18+fLyzxF3ZVMjEyDCiEDjMYZZS5wiPXnyZFbJaxMhQIQRGzHvWR7XCyOCXsOmiDAi1HmPMMQjDpbpEiDCiL358eNHurW/5SnWdIBbXiDCiA38/Pnzrce2YyZ4//59F3ePLNMl4PbpiL2J0L979+7yDtHDhw8vtzzvdGnEXdvUigSIsCLAWavHp/+qM0BcXMd/q25n1vF57TYBp0a3mUzilePj4+7k5KSLb6gt6ydAhPUzXnoPR0dHl79WGTNCfBnn1uvSCJdegQhLI1vvCk+fPu2ePXt2tZOYEV6/fn31dz+shwAR1sP1cqvLntbEN9MxA9xcYjsxS1jWR4AIa2Ibzx0tc44fYX/16lV6NDFLXH+YL32jwiACRBiEbf5KcXoTIsQSpzXx4N28Ja4BQoK7rgXiydbHjx/P25TaQAJEGAguWy0+2Q8PD6/Ki4R8EVl+bzBOnZY95fq9rj9zAkTI2SxdidBHqG9+skdw43borCXO/ZcJdraPWdv22uIEiLA4q7nvvCug8WTqzQveOH26fodo7g6uFe/a17W3+nFBAkRYENRdb1vkkz1CH9cPsVy/jrhr27PqMYvENYNlHAIesRiBYwRy0V+8iXP8+/fvX11Mr7L7ECueb/r48eMqm7FuI2BGWDEG8cm+7G3NEOfmdcTQw4h9/55lhm7DekRYKQPZF2ArbXTAyu4kDYB2YxUzwg0gi/41ztHnfQG26HbGel/crVrm7tNY+/1btkOEAZ2M05r4FB7r9GbAIdxaZYrHdOsgJ/wCEQY0J74TmOKnbxxT9n3FgGGWWsVdowHtjt9Nnvf7yQM2aZU/TIAIAxrw6dOnAWtZZcoEnBpNuTuObWMEiLAx1HY0ZQJEmHJ3HNvGCBBhY6jtaMoEiJB0Z29vL6ls58vxPcO8/zfrdo5qvKO+d3Fx8Wu8zf1dW4p/cPzLly/dtv9Ts/EbcvGAHhHyfBIhZ6NSiIBTo0LNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiECRCjUbEPNCRAhZ6NSiAARCjXbUHMCRMjZqBQiQIRCzTbUnAARcjYqhQgQoVCzDTUnQIScjUohAkQo1GxDzQkQIWejUogAEQo121BzAkTI2agUIkCEQs021JwAEXI2KoUIEKFQsw01J0CEnI1KIQJEKNRsQ80JECFno1KIABEKNdtQcwJEyNmoFCJAhELNNtScABFyNiqFCBChULMNNSdAhJyNSiEC/wGgKKC4YMA4TAAAAABJRU5ErkJggg=="
                                />
                            <p>Picture Name : {selectedArt.title  || 'N/A'}</p>
                            {console.log('selected ', selectedArt)}
                            <p>Picture Year : {selectedArt.accessionYear  || 'N/A' }</p>
                            <p>Artist : {selectedArt.artistDisplayName || 'N/A'}</p>
                            <p>Artist Nationality : {selectedArt.artistNationality  || 'N/A'}</p>
                            <p>Artist Begin Date: {selectedArt.objectBeginDate  || 'N/A'}</p>
                            <p>Artist End Date : {selectedArt.objectEndDate  || 'N/A' }</p>
                        </Drawer>
                    </div>
                </div>
                <div className="footer">

                Copyright © 2022 
                </div>
            </div>
        </div>
    );
}

export default App;

