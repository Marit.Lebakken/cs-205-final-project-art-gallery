# CS 205 Final Project - Art Gallery
## Authors 
Nick Hanna, Jialong Ke, Marit Lebakken, Adalia Williams 
## Project Details 
- Things of note:
- To use app, go to link provided in the final deliverable writeup, go to search bar, type in what you want, and hit "Search"
- Type in something like "van gogh", "blue", "green", and it will query the The Metropolitan Museum of Art database of pictures
- Be patient, the pictures gotten by the API may not be meant for webpage display, so it may take a while to render
- Do not search anything while previous request is rendering, for best results
- click on desired pictures to get prview and more information about them
- click on the picture inside the right pane for max-view
- Drop Down check list has no effect; was used in debugging
## Dependencies 
- Install flask `pip install Flask`
- Install MUI `npm install @mui/material @emotion/react @emotion/styled`
- To run in local host, follow instructions located here: 
    https://reactjs.org/community/support.html
    https://www.youtube.com/watch?v=HWpjpq2ux04&ab_channel=CBTNuggets
Make sure that you do this from the front-end folder
Back end folder is unused, this was approved by the Professor and 
